*pico adxl*
===========
I have a Klipperized FLSun SR that I wanted to set up input shaping on, and
after like two hours of crimping and fiddling around with dupont connectors I
decided to have a fully-integrated RP2040 + ADXL345 board fabricated to save
myself some grief in lieu of a different, more satisfying type of grief.

this board was designed in JLCPCB/LCSC's [EasyEDA Pro][easyeda-pro], and the
repo contains gerbers + a BOM and pick-and-place file that should be ready to
fabricate through JLCPCB's turnkey assembly service (possibly other fabs, but
no guarantees.)  the smallest component on the board is 0402 for the sake of
board size, so professional assembly is recommended, but if you like a challenge
I can't stop you.

[easyeda-pro]: https://pro.easyeda.com

programming and configuration
-----------------------------
1. follow the steps under "compile the firmware" in
   [this thread][klipper-discourse-rp2040].
2. copy the `klipper.uf2` file from `~/klipper/out` onto whichever computer
   you're going to program the *pico adxl* with.
3. short J1 with a pair of tweezers as you plug the board into the USB port on
   your computer.
4. it should be recognized as a USB drive; copy `klipper.uf2` to that drive.
5. after it reboots, remove the *pico adxl* from your computer and plug it into
   your 3D printer.
6. shell into it and find the ID of your RP2040; it should be at
   `/dev/serial/by-id/usb-Klipper_rp2040_[...]-if00`.
7. mount the *pico adxl* on your printer.  note the axis mapping -- which
   direction the axes of your printer point, with respect to the accelerometer.
   the silkscreened legend on the board indicates the positive direction of
   each accelerometer axis (nb: the silkscreen on revision A is misprinted, and
   positive z points up out of the board, not down into it.) on my printer, the
   bottom of the board faces left (-y), and the mounting hole faces down (-z),
   so the printer's three axes point toward accelerometer +z, -x, and +y
   respectively, and the axis mapping is `z, -x, y`.
8. follow the guide on the [Measuring Resonances page of the Klipper documentation][measuring-resonances]
   and choose a probe point that's appropriate for your printer.
7. create a config file called `pico-adxl.cfg` containing:

```ini
[mcu pico]
serial: /dev/serial/by-id/usb-Klipper_rp2040_[...]-if00

[adxl345]
spi_bus: spi0c
cs_pin: pico:gpio17
axes_map: (axis mapping)

[resonance_tester]
accel_chip: adxl345
probe_points:
  (probe point)
```

8. add the line `[include pico-adxl.cfg]` somewhere in your `printer.cfg` file.
9. save your configs and firmware restart, then follow the instructions in the
   documentation to calibrate input shaping.
10. enjoy your higher speeds and accelerations!

[klipper-discourse-rp2040]: https://klipper.discourse.group/t/raspberry-pi-pico-adxl345-portable-resonance-measurement/1757
[measuring-resonances]: https://www.klipper3d.org/Measuring_Resonances.html

licensing
---------
this board design is CC-NC-BY-SA; it is not Open Hardware.  you're welcome to
fabricate a few units of it for yourself or your friends, or to make a large
production run that you sell on at cost.  if you want to sell it at a profit,
please get in contact with me first and I'd be happy to work with you.

if you fabricate the design unmodified, please preserve the design attribution
silkscreen in the corner; if you design a derivative work, a mention in the
readme is enough attribution.

thanks
------
* [Nero 3D][nero-3d] for first putting the idea of the RP2040 as a host MCU on
  my radar, and Sineos on the Klipper Discourse forum for doing a lot of the
  pinout legwork
* [Teaching Tech][teaching-tech] for sending me down the rabbit hole of Klipper
  in the first place
* the Klipper developers for making a great 3D printer firmware

[nero-3d]: https://www.youtube.com/c/Nero3D
[teaching-tech]: https://www.youtube.com/channel/UCbgBDBrwsikmtoLqtpc59Bw